export const languages = [
  { name: 'en_GB', title: 'English (UK)' },
  { name: 'no', title: 'Norwegian' },
];

export const baseLanguage = languages[0]